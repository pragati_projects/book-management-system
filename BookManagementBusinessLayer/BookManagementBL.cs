﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementDataAccess;
using BookEntites;

namespace BookManagementBusinessLayer
{
    public class BookManagementBL
    {
        public static string AddDetails(int id, string name, string genre, float price)
        {
            string msg = BookManagementDA.InsertField(id,name,genre,price);
            return msg;
        }

        public static string UserDetails(int userId, string userName, int bookId)
        {
            string msg = BookManagementDA.UserField(userId, userName, bookId);
            return msg;
        }

        public static string DisplayDetails(Book bookobj)
        {
            string msg = BookManagementDA.RetriewAll(bookobj);
            return msg;
        }

        public static string DisplayIssuedBooksById(int userId)
        {
            string msg = BookManagementDA.DisplayById(userId);
            return msg;
        }
    }
}
