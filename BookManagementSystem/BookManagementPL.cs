﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementBusinessLayer;
using BookEntites;

namespace BookManagementPresentationLayer
{
    class BookManagementPL
    {

       static void Menu()
        {
            Console.WriteLine("Enter 1 to Add Book the details");
            Console.WriteLine("Enter 2 to add User Details");
            Console.WriteLine("Enter 3 to Display all the details");
            Console.WriteLine("Enter 4 to display issued books based on userid");
            Console.WriteLine("Enter 5 to display 1st 2 books from table and enter into text file");

        }

        public static void AddDetails()
        {
            Book bookobj = new Book();

            Console.WriteLine("Enter book id:");
            bookobj.BookId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter book name:");
            bookobj.BookName = Console.ReadLine();
            Console.WriteLine("Enter genre");
            bookobj.Genre = Console.ReadLine();
            Console.WriteLine("Enter price:");
            bookobj.Price = Convert.ToSingle( Console.ReadLine());
            string msg = BookManagementBL.AddDetails(bookobj.BookId, bookobj.BookName, bookobj.Genre, bookobj.Price);
            Console.WriteLine(msg);
        }

        public static void UserDetails()
        {
            User userobj = new User();

            Console.WriteLine("Enter user id:");
            userobj.UserId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter user name:");
            userobj.UserName = Console.ReadLine();
          
            Console.WriteLine("Enter book id:");
            userobj.BookId = Convert.ToInt32(Console.ReadLine());
            string msg = BookManagementBL.UserDetails(userobj.UserId, userobj.UserName,  userobj.BookId);
            Console.WriteLine(msg);
        }
        public static void DisplayDetails()
        {
            Book bookobj = new Book();
            string msg = BookManagementBL.DisplayDetails( bookobj);
            Console.WriteLine(msg);
        }
        public static void DisplayIssuedBooksById()
        {
            User userobj = new User();
            Console.WriteLine("Enter user id:");
            userobj.UserId = Convert.ToInt32(Console.ReadLine());
            string msg = BookManagementBL.DisplayIssuedBooksById(userobj.UserId);
            Console.WriteLine(msg);
        }


        static void Main(string[] args)
        {
            int choice;
            do
            {
                Menu();

                Console.WriteLine("Enter your choice:");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        AddDetails();
                        break;
                    case 2:
                        UserDetails();
                        break;

                    case 3:
                        DisplayDetails();
                        break;

                    case 4:
                        DisplayIssuedBooksById();
                        break;

                    case 5:
                        Console.WriteLine("EXITING....");
                        break;

                    default:
                        Console.WriteLine("INVALID CHOICE...");
                        break;
                }
            } while (choice != 5);

            Console.ReadLine();
        }

       
    }
}
