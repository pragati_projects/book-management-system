﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookEntites
{
    public class User
    {
        public User()
        {

        }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int BookId { get; set; }
        public  DateTime IssuedDate { get; set; }
    }
}
