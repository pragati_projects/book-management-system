﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BookEntites;


namespace BookManagementDataAccess
{
    public class BookManagementDA
    {
        static SqlConnection con = new SqlConnection("data source=.; database= Student; trusted_connection=true");
        public static string InsertField(int id, string name, string genre, float price)
        {
            try
            {

                
                SqlCommand command;

                string sqlQuery = "spBooksEntry";
               

                command = new SqlCommand(sqlQuery, con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                con.Open();
                SqlParameter parameter;
                parameter = command.Parameters.Add("@BookId", System.Data.SqlDbType.Int, 10);
                parameter.Value = id;
                parameter = command.Parameters.Add("@BookName", System.Data.SqlDbType.VarChar, 50);
                parameter.Value = name;
                parameter = command.Parameters.Add("@Genre", System.Data.SqlDbType.VarChar, 40);
                parameter.Value = genre;
                parameter = command.Parameters.Add("@Price", System.Data.SqlDbType.Float, 10);
                parameter.Value = price;

                command.ExecuteNonQuery();



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return "Rows affected";



        }

        public static string UserField(int userId, string userName, int bookId)
        {
            try
            {


                SqlCommand command;

                string sqlQuery = "spUserEntry";


                command = new SqlCommand(sqlQuery, con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                con.Open();
                SqlParameter parameter;
                parameter = command.Parameters.Add("@UserId", System.Data.SqlDbType.Int, 10);
                parameter.Value = userId;
                parameter = command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar, 50);
                parameter.Value = userName;
                parameter = command.Parameters.Add("@BookId", System.Data.SqlDbType.VarChar, 40);
                parameter.Value = bookId;
               

                command.ExecuteNonQuery();



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return "Rows affected";
        }
        public static string RetriewAll(Book bookobj)
        {
            try
            {
                
                SqlCommand command;
                string querystring = string.Format("spDisplayBooks");
              
                command = new SqlCommand("spDisplayBooks", con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(reader[0].ToString() + " " + reader[1].ToString() + " " + reader[2].ToString()+ " " + reader[3].ToString());
                    }
                }
                else 
                {
                    return "No Rows Found";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                con.Close();
            }

            return "Rows Affected";

        }
        public static string DisplayById(int userId)
        {
            try
            {
               

                SqlCommand command;

                string sqlQuery = string.Format("spDisplayBooksById");
                command = new SqlCommand(sqlQuery, con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                con.Open();
                SqlParameter parameter;
                parameter = command.Parameters.Add("@UserId", System.Data.SqlDbType.Int, 10);
                parameter.Value = userId;
                SqlDataReader rdr = command.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        Console.WriteLine("{0}", rdr[0]);
                    }
                }
                else
                {
                    return "No Rows Found";
                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                con.Close();
            }
            return "Rows affected";
        }

    }
}

